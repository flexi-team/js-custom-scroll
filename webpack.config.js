'use strict';

const HOST = process.env.HOST || 'localhost';
const PORT = process.env.PORT || 3012;

const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

var UglifySettings;
if (process.env.NODE_ENV === "production") {

    UglifySettings = {
        output: {
            comments: false
        },
        compress: {
            warnings: false,
            conditionals: true,
            unused: true,
            comparisons: true,
            sequences: true,
            dead_code: true,
            evaluate: true,
            if_return: true,
            join_vars: true,
            negate_iife: false
        },
        sourceMap: true
    };
}


var config = {
    context: __dirname,
    entry: [
        './index',
        './src/css/jsCustomScroll.scss',
    ],
    output: {
        path: __dirname + '/dist',
        filename: 'js-custom-scroll.min.js',
        publicPath: "/dist",
    },
    module: {
        rules: [
            {test: /\.js$/, loader: 'babel-loader'},
            {test: /\.css$/, use: ['style-loader', 'css-loader'], },
            {
                test: /\.(sass|scss)$/,
                loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader', 'autoprefixer-loader?browsers=last 5 version'])
            },
            {test: /\.json$/, use: "json-loader"},
            {test: /\.html$/, use: 'raw-loader', },
        ]
    },
    //To run development server
    devServer: {
        port: PORT,
        host: HOST,
        contentBase: __dirname + '/dist'
    },

    devtool: process.env.NODE_ENV === "production" ? "source-map" : "eval-source-map",

    plugins: [
        new ExtractTextPlugin({
            filename: './js-custom-scroll.css',
            allChunks: true,
        }),
        new UglifyJSPlugin(UglifySettings || '')
    ]
};


module.exports = config;